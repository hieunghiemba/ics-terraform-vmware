# How to deploy VMware vSphere Virtual Machines using Terraform

## Files

* **main.tf** --> create virtual machines

* **variables.tf** --> variables file

* **terraform.tfvars** --> update vSphere credentials and other settings



## Deployment

* Initialize providers: terraform init
* Validate configuration: terraform validate
* Update variables as needed in `terraform.tfvars`
* Preview changes:
   - PowerShell: `terraform plan --var-file=terraform.tfvars`
* Create/Update an environment:
   - PowerShell: `terraform apply --var-file=terraform.tfvars --auto-approve`
* Destroy the environment:
  - PowerShell: `terraform destroy --var-file=terraform.tfvars --auto-approve`
